/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/

/* [] END OF FILE */
#include <project.h>
#include <stdio.h>
#include <Details.h>


uint8 temp1[4];

#define InDataStartPosition 10u
#define RowNumPos 10
#define NumOfColPos 11
#define StartLutPos 12
#define TIDataLength 3
#define TimerCounter 24000000 //1 sec

void convert_by_division(int value, uint8 *temp);
void BuildHeade();
uint8 FrameNum(enum Category category);
void ClearBufOut();
uint8_t WriteInt32(uint32_t from, uint8_t* to);
uint8_t WriteFloat(float from, uint8_t* to);
uint8_t WriteInt8(uint8_t from, uint8_t* to);
uint8_t WriteInt16(uint16_t from, uint8_t* to);
int ReadFloat(void* from, float* to);
int ReadInt32(const uint8_t* from, unsigned long* to);
int ReadInt16(const uint8_t* from, uint16_t* to);
int ReadInt8(const uint8_t* from, uint8_t* to);


///////////////////USB-OUT//////////////
void SendFWVersion(struct FW_Details* FWversion, enum Status status);
void SendEBoxParams(struct EBoxParams* params, enum Status status);
void SendMatParams(struct MatParams* params, enum Status status);
uint8 SendLutToUSB(struct Lut_temp* lut,uint8 numOfCols, enum Status status);
void SendCheckSumResponse (enum SubType subtype, enum Status status, uint8 checksumresults);
void SendRowMeas(uint8_t currentRow,uint8_t NumOfColsWRef);
void SendResponse (enum SubType subtype, enum Status status);

////////////////////USB-IN//////////////
void GetPowerConfigFromUSB(struct HwcPowerConfig* powerConfig);
enum PowerStatus GetPowerStatusFromUSB();
void GetMatParamFromUSB(struct MAT_parameters* MAT_Parameters);
void GetLutFromUSB(struct MAT_Lut* mat_lut);

///////////////SPIM Write///////////////////
void WriteMatParamsToFlash(struct MAT_parameters* MAT_Parameters);
void WriteRefRowToFlash (struct MAT_RefRow* Mat_refRow);
void WriteRefColToFlash (struct MAT_RefCol* Mat_refCol);
///////////////SPIM READ///////////////////
void ReadMatParamsFromFlash( struct MAT_parameters* MAT_Parameters);
void ReadRefRowFromFlash( struct MAT_RefRow* MAT_refrow);
void ReadRefColFromFlash( struct MAT_RefCol* MAT_refcol);

/////////////////To TI Micro ////////////////////
enum I2C_Status RequestFromTI(enum I2C_Category i2cCategory ,enum I2C_ON_OFF i2cState);
enum I2C_Status LineSwitching(enum I2C_Category i2cCategory, uint8 lineNumber);
enum I2C_Status GetTIFWVversion(uint8* fwVersion);
enum I2C_Status SendLastOfCol(uint8 data);
#define TI_MicroAddress  0x48


#define USBUART_BUFFER_SIZE (64u)
uint8 bufferIN[USBUART_BUFFER_SIZE];
uint8 bufferOUT[1000];
#define basicMsgSize 10u

//uint8 responseFrameNum;
//uint8 eventFrameNum;
//uint8 requestFrameNum;





