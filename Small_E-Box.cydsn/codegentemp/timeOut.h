/*******************************************************************************
* File Name: timeOut.h
* Version 1.70
*
*  Description:
*   Provides the function definitions for the Interrupt Controller.
*
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/
#if !defined(CY_ISR_timeOut_H)
#define CY_ISR_timeOut_H


#include <cytypes.h>
#include <cyfitter.h>

/* Interrupt Controller API. */
void timeOut_Start(void);
void timeOut_StartEx(cyisraddress address);
void timeOut_Stop(void);

CY_ISR_PROTO(timeOut_Interrupt);

void timeOut_SetVector(cyisraddress address);
cyisraddress timeOut_GetVector(void);

void timeOut_SetPriority(uint8 priority);
uint8 timeOut_GetPriority(void);

void timeOut_Enable(void);
uint8 timeOut_GetState(void);
void timeOut_Disable(void);

void timeOut_SetPending(void);
void timeOut_ClearPending(void);


/* Interrupt Controller Constants */

/* Address of the INTC.VECT[x] register that contains the Address of the timeOut ISR. */
#define timeOut_INTC_VECTOR            ((reg32 *) timeOut__INTC_VECT)

/* Address of the timeOut ISR priority. */
#define timeOut_INTC_PRIOR             ((reg8 *) timeOut__INTC_PRIOR_REG)

/* Priority of the timeOut interrupt. */
#define timeOut_INTC_PRIOR_NUMBER      timeOut__INTC_PRIOR_NUM

/* Address of the INTC.SET_EN[x] byte to bit enable timeOut interrupt. */
#define timeOut_INTC_SET_EN            ((reg32 *) timeOut__INTC_SET_EN_REG)

/* Address of the INTC.CLR_EN[x] register to bit clear the timeOut interrupt. */
#define timeOut_INTC_CLR_EN            ((reg32 *) timeOut__INTC_CLR_EN_REG)

/* Address of the INTC.SET_PD[x] register to set the timeOut interrupt state to pending. */
#define timeOut_INTC_SET_PD            ((reg32 *) timeOut__INTC_SET_PD_REG)

/* Address of the INTC.CLR_PD[x] register to clear the timeOut interrupt. */
#define timeOut_INTC_CLR_PD            ((reg32 *) timeOut__INTC_CLR_PD_REG)


#endif /* CY_ISR_timeOut_H */


/* [] END OF FILE */
