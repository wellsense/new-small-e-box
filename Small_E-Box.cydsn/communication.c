/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/

/* [] END OF FILE */
#include <Communication.h>
#include <project.h>
#include <stdio.h>
#include <Flash.h>

//#include <Details.h>
//void BuildHeader ();
//uint8 FrameNum(enum Category category);
//void SendResponse (enum SubType subtype, enum Category category, enum Status status);
//#define USBUART_BUFFER_SIZE (64u)
//uint8 bufferIN[USBUART_BUFFER_SIZE];
//uint8 bufferOUT[USBUART_BUFFER_SIZE];
uint8 responseFrameNum;
uint8 eventFrameNum;
uint8 requestFrameNum;
uint8 EventNoResponseFrameNum;
uint16 size;
uint16 msg_length;
enum Status status;
enum SubType subtype;
enum Category category;  
uint8 ZeroBuf[USBUART_BUFFER_SIZE]={[0 ... 63] = 0};
uint8 I2CBufWrite[TIDataLength];
uint8 I2CBufRead[TIDataLength];
    
void BuildHeader () {
    bufferOUT[0]=0xaa;
    bufferOUT[1]=0xbb;
    bufferOUT[2]=0xbb;
    bufferOUT[3]=0xaa;
}

uint8 FrameNum(enum Category category){
    if (category==kResponse){
     return responseFrameNum++; //%msgMaxsize ?
    }
    else if (category==kEvent){
     return eventFrameNum++; 
    }
    else if (category==kRequest){
    return requestFrameNum++;
    }
    else if (category==kEventNoResponse){
    return EventNoResponseFrameNum++;
    }
    return 0;
}

//////////////////////////////////////////////////
//////////////////USB-OUT////////////////////////
////////////////////////////////////////////////

void SendFWVersion(struct FW_Details* FWversion, enum Status status){   
   // uint8 size=3; //sizeof (FWversion)
    size = sizeof(struct FW_Details);  
    memcpy(bufferOUT,FWversion,size);
    CyDelay(1000);
    for(uint8 i=0; i<size; i++){
      bufferOUT[basicMsgSize + size -1 -i] =  bufferOUT[size-1-i];
    }
    BuildHeader();
    uint16 msg_length= basicMsgSize+size;
    bufferOUT[4]=FrameNum(kResponse);  //msg serial number
    bufferOUT[5]=msg_length&0x00ff; //msg length LSB
    bufferOUT[6]=msg_length>>8; //msg length MSB 
    bufferOUT[7]=kResponse; //category
    bufferOUT[8]=subtype;    
    bufferOUT[9]=status;
    /* Wait until component is ready to send data to host. */
    while (0u == USBUART_CDCIsReady()){}                  
    USBUART_PutData(bufferOUT,msg_length);    
    ClearBufOut();
    UART_PutString("Send FW Version");
    UART_PutString("\n\r\n\r");
}

void SendResponse (enum SubType subtype, enum Status status){
    BuildHeader();
    bufferOUT[4]=FrameNum(kResponse);  //msg serial number
    bufferOUT[5]=10u; //msg length LSB
    bufferOUT[6]=0u; //msg length MSB 
    bufferOUT[7]=kResponse; //category
    bufferOUT[8]= subtype;    
    bufferOUT[9]=status;

    /* Wait until component is ready to send data to host. */
    while (0u == USBUART_CDCIsReady()){}                  
    USBUART_PutData(bufferOUT,basicMsgSize);
    ClearBufOut();
    UART_PutString("Send Response");
    UART_PutString("\n\r\n\r");
}

void SendCheckSumResponse (enum SubType subtype, enum Status status, uint8 checksumresults){
    BuildHeader();       
    bufferOUT[4]=FrameNum(kResponse);  //msg serial number
    bufferOUT[5]=11u; //msg length LSB
    bufferOUT[6]=0u; //msg length MSB 
    bufferOUT[7]=kResponse; //category
    bufferOUT[8]= subtype;    
    bufferOUT[9]=status;
    bufferOUT[10]=checksumresults;

    /* Wait until component is ready to send data to host. */
    while (0u == USBUART_CDCIsReady()){}                  
    USBUART_PutData(bufferOUT,11);
    ClearBufOut();
    UART_PutString("Send check sum Response");
    UART_PutString("\n\r\n\r");
}

void SendEBoxParams(struct EBoxParams* params, enum Status status){      
    size = sizeof(struct EBoxParams);
    msg_length=basicMsgSize+size; 
    memcpy(bufferOUT,params,size);
    CyDelay(1000);
    for(uint8 i=0; i<size; i++){
      bufferOUT[basicMsgSize + size -1 -i] =  bufferOUT[size-1-i];
    }
    BuildHeader();
    bufferOUT[4]=FrameNum(kResponse);  //msg serial number
    bufferOUT[7]=kResponse; //category
    bufferOUT[8]=kGetEBoxParamsResponse;    
    bufferOUT[9]=status;  
    bufferOUT[5]=msg_length&0x00ff; //msg length LSB
    bufferOUT[6]=msg_length>>8; //msg length MSB 
    /* Wait until component is ready to send data to host. */
    while (0u == USBUART_CDCIsReady()){}                  
    USBUART_PutData(bufferOUT,msg_length);    
    ClearBufOut();
    UART_PutString("Send E-Box Params");
    UART_PutString("\n\r\n\r");
}

void SendMatParams(struct MatParams* params, enum Status status){
    size = sizeof(struct MatParams);
    msg_length=basicMsgSize+size; 
    memcpy(bufferOUT,params,size);
    CyDelay(1000);
    for(uint8 i=0; i<size; i++){
      bufferOUT[basicMsgSize + size -1 -i] =  bufferOUT[size-1-i];
    }
    BuildHeader();
    bufferOUT[4]=FrameNum(kResponse);  //msg serial number
    bufferOUT[7]=kResponse; //category
    bufferOUT[8]=kGetMatParamsAtOnceResponse;    
    bufferOUT[9]=status;    
    bufferOUT[5]=msg_length&0x00ff; //msg length LSB
    bufferOUT[6]=msg_length>>8; //msg length MSB 
    /* Wait until component is ready to send data to host. */
    while (0u == USBUART_CDCIsReady()){}                  
    USBUART_PutData(bufferOUT,msg_length);
    ClearBufOut();
    UART_PutString("Send MAT Params");
    UART_PutString("\n\r\n\r");  
}


 uint8 SendLutToUSB(struct Lut_temp* lut,uint8 numOfCols, enum Status status){
    size = lut->numOfCoef*numOfCols*sizeof(float)+sizeof(uint32); //size of num of coef (uint16) and current row (uint16)
    msg_length=basicMsgSize+size; 
    memcpy(bufferOUT,lut,size);
    
    //CyDelay(1000);
    for(uint16 i=0; i<size; i++){
      bufferOUT[basicMsgSize + size -1 -i] =  bufferOUT[size-1-i];
    }
    uint16 hasWriten=0;
    uint16 toWrite=0;
    BuildHeader();
    bufferOUT[4]=FrameNum(kResponse);  //msg serial number
    bufferOUT[5]=msg_length&0x00ff; //msg length LSB
    bufferOUT[6]=msg_length>>8; //msg length MSB 
    bufferOUT[7]=kResponse; //category
    bufferOUT[8]=kGetLutAtOnceResponse;
    bufferOUT[9]=status;         
    
	while(hasWriten < msg_length) {
    	if(msg_length - hasWriten > USBUART_BUFFER_SIZE) {
    		toWrite = USBUART_BUFFER_SIZE;
    	}
    	else {
    		toWrite = msg_length - hasWriten;
    	}    
    while (0u == USBUART_CDCIsReady()){}    // Wait until component is ready to send data to host.               
    USBUART_PutData(&bufferOUT[hasWriten],toWrite);    
    hasWriten += toWrite;
    }
    ClearBufOut();
    UART_PutString("Send Lut To USB Params");
    UART_PutString("\n\r\n\r"); 
    return 1;
    
}

void SendRowMeas(uint8_t currentRow,uint8_t NumOfColsWRef){
    msg_length = basicMsgSize + NumOfColsWRef*3 + 1; //TBD
    uint8 hasWriten=0;
    uint8 toWrite=0;
    BuildHeader();
    bufferOUT[4]=FrameNum(kEventNoResponse);  //msg serial number
    bufferOUT[5]=msg_length&0x00ff; //msg length LSB
    bufferOUT[6]=msg_length>>8; //msg length MSB 
    bufferOUT[7]=kEventNoResponse; //category
    bufferOUT[8]=kReturnRow;
    bufferOUT[9]=currentRow;
    bufferOUT[10]=NumOfColsWRef;     
    
	while(hasWriten < msg_length) {
    	if(msg_length - hasWriten > USBUART_BUFFER_SIZE) {
    		toWrite = USBUART_BUFFER_SIZE;
    	}
    	else {
    		toWrite = msg_length - hasWriten;
    	}    
    while (0u == USBUART_CDCIsReady()){}    // Wait until component is ready to send data to host.               
    USBUART_PutData(&bufferOUT[hasWriten],toWrite);    
    hasWriten += toWrite;
    }
    ClearBufOut();
  //  UART_PutString(" Send Row Meas Params");
  //  UART_PutString("\n\r\n\r");  

}

///////////////////////////////////////////////////////
///////////////////////SPIM - Flash///////////////////
/////////////////////////////////////////////////////

uint8 SPIM_status;
uint8 SPIMWriteBuf[256];
uint8 SPIMReadBuf[256];
uint8 SPIMCommandBuf[4];
uint8 SPIMReadBuf_temp[10];


///////////////SPIM Write///////////////////
void WriteMatParamsToFlash_old( struct MatParams* MAT_Params){
    uint16 i;
    uint32 address = MatParamStartAddress;
    
    memcpy(SPIMWriteBuf,&MAT_Params,sizeof(MAT_Params));
    for(i=0;i<sizeof(MAT_Params); i++){
    SPIMWriteBuf[sizeof(MAT_Params)+4-i]=SPIMWriteBuf[sizeof(MAT_Params)-i];   
    }
    SPIM_WriteTxData(WREN);
    CyDelay(100);
    SPIM_WriteTxData(ULBPR);
    CyDelay(100);
    SPIM_WriteTxData(WREN);
    CyDelay(100);
    SPIM_WriteTxData(Erase_Full_Array);
    CyDelay(300);
    SPIM_WriteTxData(WREN);
    CyDelay(100);
    
    SPIMWriteBuf[0]=(uint8)PP;
    SPIMWriteBuf[1]=(address>16)&0x00ff; //MSB
    SPIMWriteBuf[2]=(address>8)&0x00ff;
    SPIMWriteBuf[3]= address&0x0000ff;  //LSB
    
    SPIM_status=SPIM_ReadTxStatus();
    CyDelay(300);
    if(SPIM_status & (SPIM_STS_SPI_DONE | SPIM_STS_SPI_IDLE))
    {              
        SPIM_PutArray(SPIMWriteBuf,(uint8)MAT_Params); 
    }
    CyDelay(2000);
        
}
void WriteMatParamsToFlash( struct MAT_parameters* MAT_Parameters){
    uint16 i;
    uint32 address = MatParamStartAddress;
    
    memcpy(SPIMWriteBuf,&MAT_Parameters,sizeof(MAT_Parameters));
    for(i=0;i<sizeof(MAT_Parameters); i++){
    SPIMWriteBuf[sizeof(MAT_Parameters)+4-i]=SPIMWriteBuf[sizeof(MAT_Parameters)-i];   
    }
    
    
    /*
    SPIMCommandBuf[0] = (uint8)Erase_4k_Of_Memory;
    SPIMCommandBuf[1] = (address>16)&0x00ff; //MSB
    SPIMCommandBuf[2] = (address>8)&0x00ff;
    SPIMCommandBuf[3] =  address&0x0000ff;  //LSB
    */
    SPIM_WriteTxData(WREN);
    CyDelay(100);
    SPIM_WriteTxData(ULBPR);
    CyDelay(100);
    SPIM_WriteTxData(WREN);
    CyDelay(100);
    SPIM_WriteTxData(Erase_Full_Array);
    CyDelay(300);
    SPIM_WriteTxData(WREN);
    CyDelay(100);
    /*
    SPIM_PutArray(SPIMCommandBuf,4); //write command   
    SPIMCommandBuf[0] = (uint8)PP;
    SPIMCommandBuf[1] = (address>16)&0x00ff; //MSB
    SPIMCommandBuf[2] = (address>8)&0x00ff;
    SPIMCommandBuf[3] =  address&0x0000ff;  //LSB
    
    CyDelay(300);
    */
    
    SPIMWriteBuf[0]=(uint8)PP;
    SPIMWriteBuf[1]=10;//(address>16)&0x00ff; //MSB
    SPIMWriteBuf[2]=10;//(address>8)&0x00ff;
    SPIMWriteBuf[3]=20;//address&0x0000ff;  //LSB
    SPIMWriteBuf[4]=8;//(uint8)(MAT_Parameters->checkSum_1)&0x0000ff;
    SPIMWriteBuf[5]=9;//(uint8)((MAT_Parameters->checkSum_1)>8)&0x0000ff;
    SPIMWriteBuf[6]=10;//(uint8)((MAT_Parameters->checkSum_1)>16)&0x0000ff;
    SPIMWriteBuf[7]=11;//(uint8)((MAT_Parameters->checkSum_1)>24)&0x0000ff;
    
    SPIM_status=SPIM_ReadTxStatus();
    CyDelay(300);
    if(SPIM_status & (SPIM_STS_SPI_DONE | SPIM_STS_SPI_IDLE))
    {  
            
        SPIM_PutArray(SPIMWriteBuf,8/*(uint8)MAT_Parameters*/); 
    }
    CyDelay(2000);
    
}
void WriteRefRowToFlash (struct MAT_RefRow* Mat_refRow){
    uint32 address = MatRefRowAddress;
    
    memcpy(SPIMWriteBuf,&Mat_refRow,sizeof(Mat_refRow));
    for(uint8 i=0;i<sizeof(Mat_refRow); i++){
    SPIMWriteBuf[sizeof(Mat_refRow)+4-i]=SPIMWriteBuf[sizeof(Mat_refRow)-i];   
    }
    SPIMWriteBuf[0]=(uint8)PP;
    SPIMWriteBuf[1]=(address>16)&0x00ff; //MSB
    SPIMWriteBuf[2]=(address>8)&0x00ff;
    SPIMWriteBuf[3]= address&0x0000ff;  //LSB
    
    /*      TBD
    SPIMCommandBuf[0] = (uint8)Erase_4k_Of_Memory;
    SPIMCommandBuf[1] = (address>16)&0x00ff; //MSB
    SPIMCommandBuf[2] = (address>8)&0x00ff;
    SPIMCommandBuf[3] =  address&0x0000ff;  //LSB
    SPIM_status=SPIM_ReadTxStatus();
    CyDelay(300);
    SPIM_WriteTxData(WREN);
    CyDelay(300);
    if(SPIM_status & (SPIM_STS_SPI_DONE | SPIM_STS_SPI_IDLE))
    {  
            
        SPIM_PutArray(SPIMWriteBuf,(uint8)Mat_refRow); 
    }
    CyDelay(2000);
    */
    
    SPIM_WriteTxData(WREN);
    CyDelay(300);
    SPIM_status=SPIM_ReadTxStatus();
    CyDelay(300);
    if(SPIM_status & (SPIM_STS_SPI_DONE | SPIM_STS_SPI_IDLE))
    {  
            
        SPIM_PutArray(SPIMWriteBuf,(uint8)Mat_refRow); 
    }
    CyDelay(2000);
    
}
void WriteRefColToFlash (struct MAT_RefCol* Mat_refCol){
    uint32 address = MatRefColAddress;
    
    memcpy(SPIMWriteBuf,&Mat_refCol,sizeof(Mat_refCol));
    for(uint8 i=0;i<sizeof(Mat_refCol); i++){
    SPIMWriteBuf[sizeof(Mat_refCol)+4-i]=SPIMWriteBuf[sizeof(Mat_refCol)-i];   
    }
    SPIMWriteBuf[0]=(uint8)PP;
    SPIMWriteBuf[1]=(address>16)&0x00ff; //MSB
    SPIMWriteBuf[2]=(address>8)&0x00ff;
    SPIMWriteBuf[3]= address&0x0000ff;  //LSB
    
    /*      TBD
    SPIMCommandBuf[0] = (uint8)Erase_4k_Of_Memory;
    SPIMCommandBuf[1] = (address>16)&0x00ff; //MSB
    SPIMCommandBuf[2] = (address>8)&0x00ff;
    SPIMCommandBuf[3] =  address&0x0000ff;  //LSB
    SPIM_status=SPIM_ReadTxStatus();
    CyDelay(300);
    SPIM_WriteTxData(WREN);
    CyDelay(300);
    if(SPIM_status & (SPIM_STS_SPI_DONE | SPIM_STS_SPI_IDLE))
    {  
            
        SPIM_PutArray(SPIMWriteBuf,(uint8)Mat_refCol); 
    }
    CyDelay(2000);
    */
    
    SPIM_WriteTxData(WREN);
    CyDelay(300);
    SPIM_status=SPIM_ReadTxStatus();
    CyDelay(300);
    if(SPIM_status & (SPIM_STS_SPI_DONE | SPIM_STS_SPI_IDLE))
    {  
            
        SPIM_PutArray(SPIMWriteBuf,(uint8)Mat_refCol); 
    }
    CyDelay(2000);
    
}
///////////////SPIM READ///////////////////
void ReadMatParamsFromFlash( struct MAT_parameters* MAT_Parameters){
    uint32 address = MatParamStartAddress;
    uint16 counter;
    uint8 head=6;
    uint8 dummy=0xff;
    
    SPIM_ClearRxBuffer();
    SPIMWriteBuf[0] = (uint8)READ;
    SPIMWriteBuf[1] = 10;//(address>16)&0x00ff; //MSB
    SPIMWriteBuf[2] = 10;//(address>8)&0x00ff;
    SPIMWriteBuf[3] = 20;// address&0x0000ff;  //LSB
    
    SPIM_PutArray(SPIMWriteBuf,4); 
    for(counter=0;counter<6/*sizeof(MAT_Parameters)*/;counter++){ 
    SPIMReadBuf_temp[counter] = SPIM_ReadRxData();  
    SPIM_PutArray(&dummy,1);         
    }
    for(counter=0;counter<10/*sizeof(MAT_Parameters)*/;counter++){ 
    SPIMReadBuf[counter] = SPIM_ReadRxData();  
    SPIM_PutArray(&dummy,1);      
    }
    counter = sizeof(struct MAT_parameters);    
    CyDelay(1000);
    memcpy(MAT_Parameters,&SPIMReadBuf,sizeof(struct MAT_parameters)); //TBD sizeof(struct MAT_parameters)=32 instead of 28
    CyDelay(1000);

}

void ReadRefRowFromFlash( struct MAT_RefRow* MAT_refRow){
    uint32 address = MatRefRowAddress;
    uint16 counter;
    uint8 head=6;
    uint8 dummy=0xff;
    
    SPIM_ClearRxBuffer();
    SPIMWriteBuf[0] = (uint8)READ;
    SPIMWriteBuf[1] = (address>16)&0x00ff; //MSB
    SPIMWriteBuf[2] = (address>8)&0x00ff;
    SPIMWriteBuf[3] =  address&0x0000ff;  //LSB
    
    SPIM_PutArray(SPIMWriteBuf,4); 
    for(counter=0;counter<6;counter++){ 
    SPIMReadBuf_temp[counter] = SPIM_ReadRxData();  
    SPIM_PutArray(&dummy,1);         
    }
    for(counter=0;counter<sizeof(MAT_refRow);counter++){ 
    SPIMReadBuf[counter] = SPIM_ReadRxData();  
    SPIM_PutArray(&dummy,1);      
    }
    counter = sizeof(struct MAT_parameters);    
    CyDelay(1000);
    memcpy(MAT_refRow,&SPIMReadBuf,sizeof(struct MAT_RefRow)); //TBD sizeof(struct MAT_RefRow)=32 instead of 28
    CyDelay(1000);

}

void ReadRefColFromFlash( struct MAT_RefCol* MAT_refcol){
    uint32 address = MatRefRowAddress;
    uint16 counter;
    uint8 head=6;
    uint8 dummy=0xff;
    
    SPIM_ClearRxBuffer();
    SPIMWriteBuf[0] = (uint8)READ;
    SPIMWriteBuf[1] = (address>16)&0x00ff; //MSB
    SPIMWriteBuf[2] = (address>8)&0x00ff;
    SPIMWriteBuf[3] =  address&0x0000ff;  //LSB
    
    SPIM_PutArray(SPIMWriteBuf,4); 
    for(counter=0;counter<6;counter++){ 
    SPIMReadBuf_temp[counter] = SPIM_ReadRxData();  
    SPIM_PutArray(&dummy,1);         
    }
    for(counter=0;counter<sizeof(MAT_refcol);counter++){ 
    SPIMReadBuf[counter] = SPIM_ReadRxData();  
    SPIM_PutArray(&dummy,1);      
    }
    counter = sizeof(struct MAT_parameters);    
    CyDelay(1000);
    memcpy(MAT_refcol,&SPIMReadBuf,sizeof(struct MAT_RefCol)); //TBD sizeof(struct MAT_RefCol)=32 instead of 28
    CyDelay(1000);

}

//////////////////////////////////////////////
/////////////////USB - IN ////////////////////
//////////////////////////////////////////////

void GetPowerConfigFromUSB(struct HwcPowerConfig* powerConfig){
    uint16 _nextParamPtr = 9;
    _nextParamPtr +=ReadFloat(&bufferIN[_nextParamPtr],&powerConfig->refreshRate);
    _nextParamPtr +=ReadFloat(&bufferIN[_nextParamPtr],&powerConfig->batteryImageDelay);
    _nextParamPtr +=ReadFloat(&bufferIN[_nextParamPtr],&powerConfig->codeRedImageDelay);    
}
enum PowerStatus GetPowerStatusFromUSB(){ 
    uint8_t state;
    uint16 _nextParamPtr = 9;
	ReadInt8(&bufferIN[_nextParamPtr],&state);
    return state;
}
void GetMatParamFromUSB(struct MAT_parameters* MAT_Parameters){
    uint16 count;

        // Check for input data from host. 
    if (0u != USBUART_DataIsReady()){
        // Read received data and re-enable OUT endpoint. 
        count = USBUART_GetAll(bufferIN);
        MAT_Parameters-> checkSum_1=10;//bufferIN[InDataStartPosition+CheckSum1Address];
        MAT_Parameters-> MT_ID=11;//bufferIN[InDataStartPosition+MT_ID_Address];
        MAT_Parameters-> NumOfRows=12;//bufferIN[InDataStartPosition+NumOfRowsAddress];
        MAT_Parameters-> NumOfCols=13;//bufferIN[InDataStartPosition+NumOfColsAddress];
        MAT_Parameters-> HWMajor=14;//bufferIN[InDataStartPosition+HWMajorAddress];
        MAT_Parameters-> HWMinor=15;//bufferIN[InDataStartPosition+HWMinorAddress];
        MAT_Parameters-> LifeTime=16;//bufferIN[InDataStartPosition+LifeTimeAddress];
        MAT_Parameters-> FirstActivationDate=17;//bufferIN[InDataStartPosition+FirstActivationDateAddress];
        MAT_Parameters-> codeRedStatus=18;//bufferIN[InDataStartPosition+codeRedStatusAddress];
        MAT_Parameters-> codeRedIndex=19;//bufferIN[InDataStartPosition+codeRedIndexAddress];
        MAT_Parameters-> ManufacturingDate=20;//bufferIN[InDataStartPosition+ManufacturingDateAddress];
        MAT_Parameters-> numOfCoef=21;//bufferIN[InDataStartPosition+numOfCoefAddress];
    }
}

void GetLutFromUSB(struct MAT_Lut* mat_lut){
    uint8 i;
    uint8 j=0;
    // Check for input data from host. 
    if (0u != USBUART_DataIsReady()){
       mat_lut->RowNum = bufferIN[RowNumPos];
       mat_lut->numOfCols = bufferIN[NumOfColPos];
       for(i=NumOfColPos+1; i<USBUART_BUFFER_SIZE; i++){
       mat_lut->coefList[j]= bufferIN[StartLutPos+j];
       j++;
       }
    }
}

/////////////////////////// TO TI Micro /////////////////////////////////
enum I2C_Status RequestFromTI(enum I2C_Category i2cCategory, enum I2C_ON_OFF i2cState){
    Timer_1_WriteCounter(TimerCounter);
    CyDelayUs(10);
    Timer_1_Start();    
    I2CBufWrite[0]=i2cCategory;
    I2CBufWrite[1]=i2cState;
    UART_PutString("TI request");
    UART_PutString("\n\r\n\r");
    uint8 I2C_Status = I2C_MasterWriteBuf(TI_MicroAddress, (uint8 *)I2CBufWrite,2, I2C_MODE_COMPLETE_XFER);
    while(I2C_MasterStatus() & I2C_MSTAT_XFER_INP);
    I2C_Status = I2C_MasterReadBuf(TI_MicroAddress, (uint8 *)I2CBufRead,2, I2C_MODE_COMPLETE_XFER);
    while(I2C_MasterStatus() & I2C_MSTAT_XFER_INP){};                    
    if(0u != (I2C_MasterStatus() & I2C_MSTAT_RD_CMPLT)){   //TBD when failed send MSg to tablet  
        if(I2CBufRead[0]!=i2cCategory || I2CBufRead[1]!=kI2C_Success ){
        UART_PutString("error comm to TI ");
        UART_PutString("\n\r\n\r"); 
        return (enum I2C_Status)kFailed; //TBD send msg to tablet
        }
    }
    Timer_1_Stop();
    return (enum I2C_Status)kSuccess; 
}

enum I2C_Status LineSwitching(enum I2C_Category i2cCategory, uint8 lineNumber){
    Timer_1_WriteCounter(TimerCounter);
    CyDelayUs(10);
    Timer_1_Start();  
    I2CBufWrite[0]=i2cCategory;
    I2CBufWrite[1]=lineNumber;

    uint8 I2C_Status = I2C_MasterWriteBuf(TI_MicroAddress, (uint8 *)I2CBufWrite, 2, I2C_MODE_COMPLETE_XFER);
    
    while(I2C_MasterStatus() & I2C_MSTAT_XFER_INP); // Wait for the data transfer to complete   
    if((I2C_MasterStatus() & I2C_MSTAT_ERR_XFER)){
        UART_PutString("I2C communication Failed ");
        UART_PutString("\n\r\n\r"); 
        return (enum I2C_Status)kFailed; //TBD send msg to tablet
    }
   ////Read response from TI
    I2C_Status = I2C_MasterReadBuf(TI_MicroAddress, (uint8 *)I2CBufRead, 3, I2C_MODE_COMPLETE_XFER);                          
    while(I2C_MasterStatus() & I2C_MSTAT_XFER_INP); // Wait for the data transfer to complete  
    if(0u != (I2C_MasterStatus() & I2C_MSTAT_RD_CMPLT)){//Wait for I2C master to complete to read             
        if(I2CBufRead[0]!=i2cCategory || I2CBufRead[1]!=lineNumber || I2CBufRead[2]!=kI2C_Success){
            UART_PutString("\n\r\n\r"); 
            UART_PutString("error while switching rows ");
            UART_PutString("\n\r\n\r"); 
           return (enum I2C_Status)kFailed; //TBD send msg to tablet
        }
    }
    Timer_1_Stop();
    return (enum I2C_Status)kSuccess;
}

enum I2C_Status GetTIFWVversion(uint8* fwVersion){
    Timer_1_WriteCounter(TimerCounter);
    CyDelayUs(10);
    Timer_1_Start();  
    I2CBufWrite[0]=kGetTIFWVersion;
    I2CBufWrite[1]=1; //don't care
    uint8 I2C_Status = I2C_MasterWriteBuf(TI_MicroAddress, (uint8 *)I2CBufWrite, 2, I2C_MODE_COMPLETE_XFER);
    while(I2C_MasterStatus() & I2C_MSTAT_XFER_INP); // Wait for the data transfer to complete   
    if((I2C_MasterStatus() & I2C_MSTAT_ERR_XFER)){
        UART_PutString("I2C communication Failed ");
        UART_PutString("\n\r\n\r");         
        return (enum I2C_Status)kFailed; //TBD send msg to tablet
    }
    I2C_Status = I2C_MasterReadBuf(TI_MicroAddress, (uint8 *)I2CBufRead, 2, I2C_MODE_COMPLETE_XFER);                          
    while(I2C_MasterStatus() & I2C_MSTAT_XFER_INP); // Wait for the data transfer to complete  
    if(0u != (I2C_MasterStatus() & I2C_MSTAT_RD_CMPLT)){//Wait for I2C master to complete to read    
        if(I2CBufRead[0]!=kGetTIFWVersion){ 
            UART_PutString("\n\r\n\r"); 
            UART_PutString("error while GetTIFWVversion");
            UART_PutString("\n\r\n\r"); 
           return (enum I2C_Status)kFailed; //TBD send msg to tablet
        }
    *fwVersion= I2CBufRead[1];
    }
    Timer_1_Stop();
    return (enum I2C_Status)kSuccess;   
}

enum I2C_Status SendLastOfCol(uint8 data){
    Timer_1_WriteCounter(TimerCounter);
    CyDelayUs(10);
    Timer_1_Start();  
    I2CBufWrite[0] = kLastCol;
    I2CBufWrite[1] = data;
    
    uint8 I2C_Status = I2C_MasterWriteBuf(TI_MicroAddress, (uint8 *)I2CBufWrite, 2, I2C_MODE_COMPLETE_XFER);
    while(I2C_MasterStatus() & I2C_MSTAT_XFER_INP); // Wait for the data transfer to complete   
    if((I2C_MasterStatus() & I2C_MSTAT_ERR_XFER)){
        UART_PutString("I2C communication Failed ");
        UART_PutString("\n\r\n\r");         
        return (enum I2C_Status)kFailed; //TBD send msg to tablet
    }
    I2C_Status = I2C_MasterReadBuf(TI_MicroAddress, (uint8 *)I2CBufRead, 2, I2C_MODE_COMPLETE_XFER);                          
    while(I2C_MasterStatus() & I2C_MSTAT_XFER_INP); // Wait for the data transfer to complete  
    if(0u != (I2C_MasterStatus() & I2C_MSTAT_RD_CMPLT)){//Wait for I2C master to complete to read    
        if(I2CBufRead[0]!=kLastCol|| I2CBufRead[1]!=kI2C_Success){ 
            UART_PutString("\n\r\n\r"); 
            UART_PutString("SendNumOfCol ");
            UART_PutString("\n\r\n\r"); 
           return (enum I2C_Status)kFailed; //TBD send msg to tablet
        }    
    }
    Timer_1_Stop();
    return (enum I2C_Status)kSuccess; 
}

////////////////////////////////////////////////////////////////////////////

uint8_t WriteFloat(float from, uint8_t* to){
	uint32_t temp;
	memcpy(&temp,&from,sizeof(float));
	WriteInt32(temp,to);
	return sizeof(float);
}
uint8_t WriteInt32(uint32_t from, uint8_t* to){
     //   uint32_t temp = from;
        memcpy(to,&from,sizeof(uint32_t));
        return sizeof(uint32_t);
}
uint8_t WriteInt8(uint8_t from, uint8_t* to){
	memcpy(to,&from,sizeof(uint8_t));
	return sizeof(uint8_t);
}
uint8_t WriteInt16(uint16_t from, uint8_t* to){
	uint16_t temp = from;
	memcpy(to,&temp,sizeof(uint16_t));
	return sizeof(uint16_t);
}
int ReadFloat(void* from, float* to){
	memcpy(to,from,sizeof(float));
	return sizeof(float);
}
int ReadInt32(const uint8_t* from, unsigned long* to){
    memcpy(to, from, sizeof(uint32_t));        
    return(sizeof(uint32_t));
}
int ReadInt16(const uint8_t* from, uint16_t* to){
	memcpy(to, from, sizeof(uint16_t));
	return(sizeof(uint16_t));
}
int ReadInt8(const uint8_t* from, uint8_t* to){
	memcpy(to,from,sizeof(uint8_t));
	return(sizeof(uint8_t));
}



void ClearBufOut(){
    while (0u == USBUART_CDCIsReady()){} 
    //memset (bufferOUT,0,USBUART_BUFFER_SIZE);  
    //CyDelay(10);
   // USBUART_PutData(ZeroBuf,USBUART_BUFFER_SIZE);
   USBUART_PutData(ZeroBuf,10);
    while (0u == USBUART_CDCIsReady()){} 
   // CyDelay(10);
}

void convert_by_division(int value, uint8 *temp)
{
    temp[3] = (value %    10)        + '0';
    temp[2] = (value %   100) /   10 + '0';
    temp[1] = (value %  1000) /  100 + '0';
    temp[0] = (value % 10000) / 1000 + '0';
}



