/*******************************************************************************
* File Name: Tab_Con.h  
* Version 2.20
*
* Description:
*  This file contains Pin function prototypes and register defines
*
* Note:
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_Tab_Con_H) /* Pins Tab_Con_H */
#define CY_PINS_Tab_Con_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"
#include "Tab_Con_aliases.h"

/* APIs are not generated for P15[7:6] */
#if !(CY_PSOC5A &&\
	 Tab_Con__PORT == 15 && ((Tab_Con__MASK & 0xC0) != 0))


/***************************************
*        Function Prototypes             
***************************************/    

/**
* \addtogroup group_general
* @{
*/
void    Tab_Con_Write(uint8 value);
void    Tab_Con_SetDriveMode(uint8 mode);
uint8   Tab_Con_ReadDataReg(void);
uint8   Tab_Con_Read(void);
void    Tab_Con_SetInterruptMode(uint16 position, uint16 mode);
uint8   Tab_Con_ClearInterrupt(void);
/** @} general */

/***************************************
*           API Constants        
***************************************/
/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup driveMode Drive mode constants
     * \brief Constants to be passed as "mode" parameter in the Tab_Con_SetDriveMode() function.
     *  @{
     */
        #define Tab_Con_DM_ALG_HIZ         PIN_DM_ALG_HIZ
        #define Tab_Con_DM_DIG_HIZ         PIN_DM_DIG_HIZ
        #define Tab_Con_DM_RES_UP          PIN_DM_RES_UP
        #define Tab_Con_DM_RES_DWN         PIN_DM_RES_DWN
        #define Tab_Con_DM_OD_LO           PIN_DM_OD_LO
        #define Tab_Con_DM_OD_HI           PIN_DM_OD_HI
        #define Tab_Con_DM_STRONG          PIN_DM_STRONG
        #define Tab_Con_DM_RES_UPDWN       PIN_DM_RES_UPDWN
    /** @} driveMode */
/** @} group_constants */
    
/* Digital Port Constants */
#define Tab_Con_MASK               Tab_Con__MASK
#define Tab_Con_SHIFT              Tab_Con__SHIFT
#define Tab_Con_WIDTH              1u

/* Interrupt constants */
#if defined(Tab_Con__INTSTAT)
/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup intrMode Interrupt constants
     * \brief Constants to be passed as "mode" parameter in Tab_Con_SetInterruptMode() function.
     *  @{
     */
        #define Tab_Con_INTR_NONE      (uint16)(0x0000u)
        #define Tab_Con_INTR_RISING    (uint16)(0x0001u)
        #define Tab_Con_INTR_FALLING   (uint16)(0x0002u)
        #define Tab_Con_INTR_BOTH      (uint16)(0x0003u) 
    /** @} intrMode */
/** @} group_constants */

    #define Tab_Con_INTR_MASK      (0x01u) 
#endif /* (Tab_Con__INTSTAT) */


/***************************************
*             Registers        
***************************************/

/* Main Port Registers */
/* Pin State */
#define Tab_Con_PS                     (* (reg8 *) Tab_Con__PS)
/* Data Register */
#define Tab_Con_DR                     (* (reg8 *) Tab_Con__DR)
/* Port Number */
#define Tab_Con_PRT_NUM                (* (reg8 *) Tab_Con__PRT) 
/* Connect to Analog Globals */                                                  
#define Tab_Con_AG                     (* (reg8 *) Tab_Con__AG)                       
/* Analog MUX bux enable */
#define Tab_Con_AMUX                   (* (reg8 *) Tab_Con__AMUX) 
/* Bidirectional Enable */                                                        
#define Tab_Con_BIE                    (* (reg8 *) Tab_Con__BIE)
/* Bit-mask for Aliased Register Access */
#define Tab_Con_BIT_MASK               (* (reg8 *) Tab_Con__BIT_MASK)
/* Bypass Enable */
#define Tab_Con_BYP                    (* (reg8 *) Tab_Con__BYP)
/* Port wide control signals */                                                   
#define Tab_Con_CTL                    (* (reg8 *) Tab_Con__CTL)
/* Drive Modes */
#define Tab_Con_DM0                    (* (reg8 *) Tab_Con__DM0) 
#define Tab_Con_DM1                    (* (reg8 *) Tab_Con__DM1)
#define Tab_Con_DM2                    (* (reg8 *) Tab_Con__DM2) 
/* Input Buffer Disable Override */
#define Tab_Con_INP_DIS                (* (reg8 *) Tab_Con__INP_DIS)
/* LCD Common or Segment Drive */
#define Tab_Con_LCD_COM_SEG            (* (reg8 *) Tab_Con__LCD_COM_SEG)
/* Enable Segment LCD */
#define Tab_Con_LCD_EN                 (* (reg8 *) Tab_Con__LCD_EN)
/* Slew Rate Control */
#define Tab_Con_SLW                    (* (reg8 *) Tab_Con__SLW)

/* DSI Port Registers */
/* Global DSI Select Register */
#define Tab_Con_PRTDSI__CAPS_SEL       (* (reg8 *) Tab_Con__PRTDSI__CAPS_SEL) 
/* Double Sync Enable */
#define Tab_Con_PRTDSI__DBL_SYNC_IN    (* (reg8 *) Tab_Con__PRTDSI__DBL_SYNC_IN) 
/* Output Enable Select Drive Strength */
#define Tab_Con_PRTDSI__OE_SEL0        (* (reg8 *) Tab_Con__PRTDSI__OE_SEL0) 
#define Tab_Con_PRTDSI__OE_SEL1        (* (reg8 *) Tab_Con__PRTDSI__OE_SEL1) 
/* Port Pin Output Select Registers */
#define Tab_Con_PRTDSI__OUT_SEL0       (* (reg8 *) Tab_Con__PRTDSI__OUT_SEL0) 
#define Tab_Con_PRTDSI__OUT_SEL1       (* (reg8 *) Tab_Con__PRTDSI__OUT_SEL1) 
/* Sync Output Enable Registers */
#define Tab_Con_PRTDSI__SYNC_OUT       (* (reg8 *) Tab_Con__PRTDSI__SYNC_OUT) 

/* SIO registers */
#if defined(Tab_Con__SIO_CFG)
    #define Tab_Con_SIO_HYST_EN        (* (reg8 *) Tab_Con__SIO_HYST_EN)
    #define Tab_Con_SIO_REG_HIFREQ     (* (reg8 *) Tab_Con__SIO_REG_HIFREQ)
    #define Tab_Con_SIO_CFG            (* (reg8 *) Tab_Con__SIO_CFG)
    #define Tab_Con_SIO_DIFF           (* (reg8 *) Tab_Con__SIO_DIFF)
#endif /* (Tab_Con__SIO_CFG) */

/* Interrupt Registers */
#if defined(Tab_Con__INTSTAT)
    #define Tab_Con_INTSTAT            (* (reg8 *) Tab_Con__INTSTAT)
    #define Tab_Con_SNAP               (* (reg8 *) Tab_Con__SNAP)
    
	#define Tab_Con_0_INTTYPE_REG 		(* (reg8 *) Tab_Con__0__INTTYPE)
#endif /* (Tab_Con__INTSTAT) */

#endif /* CY_PSOC5A... */

#endif /*  CY_PINS_Tab_Con_H */


/* [] END OF FILE */
