/*******************************************************************************
* File Name: Col_24.h  
* Version 2.20
*
* Description:
*  This file contains Pin function prototypes and register defines
*
* Note:
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_Col_24_H) /* Pins Col_24_H */
#define CY_PINS_Col_24_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"
#include "Col_24_aliases.h"

/* APIs are not generated for P15[7:6] */
#if !(CY_PSOC5A &&\
	 Col_24__PORT == 15 && ((Col_24__MASK & 0xC0) != 0))


/***************************************
*        Function Prototypes             
***************************************/    

/**
* \addtogroup group_general
* @{
*/
void    Col_24_Write(uint8 value);
void    Col_24_SetDriveMode(uint8 mode);
uint8   Col_24_ReadDataReg(void);
uint8   Col_24_Read(void);
void    Col_24_SetInterruptMode(uint16 position, uint16 mode);
uint8   Col_24_ClearInterrupt(void);
/** @} general */

/***************************************
*           API Constants        
***************************************/
/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup driveMode Drive mode constants
     * \brief Constants to be passed as "mode" parameter in the Col_24_SetDriveMode() function.
     *  @{
     */
        #define Col_24_DM_ALG_HIZ         PIN_DM_ALG_HIZ
        #define Col_24_DM_DIG_HIZ         PIN_DM_DIG_HIZ
        #define Col_24_DM_RES_UP          PIN_DM_RES_UP
        #define Col_24_DM_RES_DWN         PIN_DM_RES_DWN
        #define Col_24_DM_OD_LO           PIN_DM_OD_LO
        #define Col_24_DM_OD_HI           PIN_DM_OD_HI
        #define Col_24_DM_STRONG          PIN_DM_STRONG
        #define Col_24_DM_RES_UPDWN       PIN_DM_RES_UPDWN
    /** @} driveMode */
/** @} group_constants */
    
/* Digital Port Constants */
#define Col_24_MASK               Col_24__MASK
#define Col_24_SHIFT              Col_24__SHIFT
#define Col_24_WIDTH              1u

/* Interrupt constants */
#if defined(Col_24__INTSTAT)
/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup intrMode Interrupt constants
     * \brief Constants to be passed as "mode" parameter in Col_24_SetInterruptMode() function.
     *  @{
     */
        #define Col_24_INTR_NONE      (uint16)(0x0000u)
        #define Col_24_INTR_RISING    (uint16)(0x0001u)
        #define Col_24_INTR_FALLING   (uint16)(0x0002u)
        #define Col_24_INTR_BOTH      (uint16)(0x0003u) 
    /** @} intrMode */
/** @} group_constants */

    #define Col_24_INTR_MASK      (0x01u) 
#endif /* (Col_24__INTSTAT) */


/***************************************
*             Registers        
***************************************/

/* Main Port Registers */
/* Pin State */
#define Col_24_PS                     (* (reg8 *) Col_24__PS)
/* Data Register */
#define Col_24_DR                     (* (reg8 *) Col_24__DR)
/* Port Number */
#define Col_24_PRT_NUM                (* (reg8 *) Col_24__PRT) 
/* Connect to Analog Globals */                                                  
#define Col_24_AG                     (* (reg8 *) Col_24__AG)                       
/* Analog MUX bux enable */
#define Col_24_AMUX                   (* (reg8 *) Col_24__AMUX) 
/* Bidirectional Enable */                                                        
#define Col_24_BIE                    (* (reg8 *) Col_24__BIE)
/* Bit-mask for Aliased Register Access */
#define Col_24_BIT_MASK               (* (reg8 *) Col_24__BIT_MASK)
/* Bypass Enable */
#define Col_24_BYP                    (* (reg8 *) Col_24__BYP)
/* Port wide control signals */                                                   
#define Col_24_CTL                    (* (reg8 *) Col_24__CTL)
/* Drive Modes */
#define Col_24_DM0                    (* (reg8 *) Col_24__DM0) 
#define Col_24_DM1                    (* (reg8 *) Col_24__DM1)
#define Col_24_DM2                    (* (reg8 *) Col_24__DM2) 
/* Input Buffer Disable Override */
#define Col_24_INP_DIS                (* (reg8 *) Col_24__INP_DIS)
/* LCD Common or Segment Drive */
#define Col_24_LCD_COM_SEG            (* (reg8 *) Col_24__LCD_COM_SEG)
/* Enable Segment LCD */
#define Col_24_LCD_EN                 (* (reg8 *) Col_24__LCD_EN)
/* Slew Rate Control */
#define Col_24_SLW                    (* (reg8 *) Col_24__SLW)

/* DSI Port Registers */
/* Global DSI Select Register */
#define Col_24_PRTDSI__CAPS_SEL       (* (reg8 *) Col_24__PRTDSI__CAPS_SEL) 
/* Double Sync Enable */
#define Col_24_PRTDSI__DBL_SYNC_IN    (* (reg8 *) Col_24__PRTDSI__DBL_SYNC_IN) 
/* Output Enable Select Drive Strength */
#define Col_24_PRTDSI__OE_SEL0        (* (reg8 *) Col_24__PRTDSI__OE_SEL0) 
#define Col_24_PRTDSI__OE_SEL1        (* (reg8 *) Col_24__PRTDSI__OE_SEL1) 
/* Port Pin Output Select Registers */
#define Col_24_PRTDSI__OUT_SEL0       (* (reg8 *) Col_24__PRTDSI__OUT_SEL0) 
#define Col_24_PRTDSI__OUT_SEL1       (* (reg8 *) Col_24__PRTDSI__OUT_SEL1) 
/* Sync Output Enable Registers */
#define Col_24_PRTDSI__SYNC_OUT       (* (reg8 *) Col_24__PRTDSI__SYNC_OUT) 

/* SIO registers */
#if defined(Col_24__SIO_CFG)
    #define Col_24_SIO_HYST_EN        (* (reg8 *) Col_24__SIO_HYST_EN)
    #define Col_24_SIO_REG_HIFREQ     (* (reg8 *) Col_24__SIO_REG_HIFREQ)
    #define Col_24_SIO_CFG            (* (reg8 *) Col_24__SIO_CFG)
    #define Col_24_SIO_DIFF           (* (reg8 *) Col_24__SIO_DIFF)
#endif /* (Col_24__SIO_CFG) */

/* Interrupt Registers */
#if defined(Col_24__INTSTAT)
    #define Col_24_INTSTAT            (* (reg8 *) Col_24__INTSTAT)
    #define Col_24_SNAP               (* (reg8 *) Col_24__SNAP)
    
	#define Col_24_0_INTTYPE_REG 		(* (reg8 *) Col_24__0__INTTYPE)
#endif /* (Col_24__INTSTAT) */

#endif /* CY_PSOC5A... */

#endif /*  CY_PINS_Col_24_H */


/* [] END OF FILE */
