/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/

/* [] END OF FILE */
/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
# include <Manager.h>
# include <Communication.h>
# include <Flash.h>
#include <stdlib.h>
//# include <communication.c>
//# include <Details.h>
//void WaitForReadyFromScu();


#define _Bool	bool	
#define true	1
#define false	0
uint16 count;
enum SubType subtype;
enum Bool ready, getPowerConfig, getPowerStatus, _running;
uint8 temp[4];
uint8 I2CBufWrite[3];
uint8 I2CBufRead[3];
struct MAT_parameters MAT_Parameters;
struct EBoxParams e_box_params;
struct MatParams mat_params;
struct HwcPowerConfig powerConfig;
enum PowerStatus powerStatus=kPowerStatusUnknown;;
struct FW_Details fwVersion;
enum I2C_Status i2cStatus;
enum I2C_Category I2C_category;
struct Lut_temp lut;
uint8 NumOfSamples = 10; //TBD
uint8 TIFWVersion;
float coef1 = -10.88;
float coef2 = 4.06e12;
float coef3 = -2.78e23;
float coef4 = 1.4e34;
enum Bool timeOut = false;
enum Bool TI_Comm_Fail = false;

 
 void StartOperation() {
    UART_PutString("In StartOperation");
    UART_PutString("\n\r\n\r");
    
   _running = true;
  //  _running = 1; 
    enum Next_State nextState;
    nextState = kIDLE;
  ///////////////////////////////////////////////////////  
 ///////////////////////////MAIN LOOP //////////////////
///////////////////////////////////////////////////////   
    while (_running) { 
        switch ( nextState ) {
            case kWaitForSelfTest:
                nextState  = WaitForSelfTest();
            break;
             case kReadyToStart:
                nextState = ReadyToStart();
            break;
            case kNormalOperation:
                nextState = RegularOperation();
            break;
            case kOverlayParams:
                nextState = OverlayParams();
                break;
            case kEBoxParams:
                nextState = EBoxParamsState();
                break;
            case kIDLE :
            	WaitForReadyFromScu();//Waits until the system is ready                
    			GetPowerConfig(); //Get the power config values
    			GetPowerStatus(); //Get the power status from SCU, AC or battery
                if(TI_Comm_Fail==true){
                nextState = kCommunicationFail;
                }
                else {
    			nextState = kWaitForSelfTest;
                }
            	break;
            case kCommunicationFail :
            	UART_PutString("CommunicationFail");
                UART_PutString("\n\r\n\r");
                if(TI_Comm_Fail==true){
                    SendResponse(kError,kFailed);//TBD define new msg ?
                    CyDelay(1000);
                }
            	NVIC_SystemReset(); //TBD decide what to do when the comm failed - IDLE/RESET ?
            //	_comm->StartOperation();
            //	NVIC_DisableIRQ(WDT_IRQn); //stop WDT
            //	nextState = kIDLE;
            	break;
            case kCodeRed:
            //	NVIC_DisableIRQ(WDT_IRQn); //stop WDT
            //	nextState = CodeRedActive();
            	break;
            default:
            //    HWC_LOG_WARN0("Illegal State, in start operation");
                break;
        }
        
        
    }
}
    
    void WaitForReadyFromScu(){
        UART_PutString("In WaitForReadyFromScu");
        UART_PutString("\n\r\n\r");
        ready=false;
        
        while(ready!=true){
                /* Host can send double SET_INTERFACE request. */
        if (0u != USBUART_IsConfigurationChanged())
        {
            /* Initialize IN endpoints when device is configured. */
            if (0u != USBUART_GetConfiguration())
            {
                /* Enumeration is done, enable OUT endpoint to receive data 
                 * from host. */
                USBUART_CDC_Init();
                CyDelay(500);
            }         
        }
        
        //if (0u != USBUART_GetConfiguration())
       // {           
            // Check for input data from host. 
            if (0u != USBUART_DataIsReady())
            {
                // Read received data and re-enable OUT endpoint. 
                count = USBUART_GetAll(bufferIN);
                subtype = bufferIN[8];
                if (subtype == kReady) //start operation
                {
                    UART_PutString("In get Ready");
                    UART_PutString("\n\r\n\r");                    
                    SendResponse(kReadyResponse,kSuccess);
                    ready=true;                   
                    
                    
                    // Get TI FW Version
                    if((GetTIFWVversion(&TIFWVersion)!=kSuccess||timeOut==true)){
                      TI_Comm_Fail=true;                      
                    }                  
                    //// comm succcess led on
                    if((RequestFromTI(kTabletLed,kOn)!=kSuccess)||timeOut==true){
                        Timer_1_Stop();
                        TI_Comm_Fail=true;                        
                    }
                    //UART_PutString("after timeout");
                    //UART_PutString("\n\r\n\r"); 
                     //////LDO 1.25 EN ////
                    if((RequestFromTI(kLDOEn,kOn)!=kSuccess||timeOut==true)){
                        Timer_1_Stop();
                        TI_Comm_Fail=true;                        
                    }                
                     
                }
                else {
                    UART_PutString("In get kInvalidRequest");
                    UART_PutString("\n\r\n\r");
                    SendResponse(subtype,kInvalidRequest);
                }
            }
        //}
        
        }
    }
    
    void GetPowerConfig(){
        UART_PutString("In GetPowerConfig");
        UART_PutString("\n\r\n\r");
        getPowerConfig=false;
        while (!getPowerConfig) {
            // Check for input data from host. 
            if (0u != USBUART_DataIsReady())
            {
                // Read received data and re-enable OUT endpoint. 
                count = USBUART_GetAll(bufferIN);
                subtype = bufferIN[8];
                if (subtype ==kPowerConfig) //start operation
                {                    
                    GetPowerConfigFromUSB(&powerConfig);
                    SendResponse(kPowerConfigResponse, kSuccess);
                    getPowerConfig=true;
                }
                else if (subtype==kReset){
                    UART_PutString("Reset was requested");
                    UART_PutString("\n\r\n\r");
                    SendResponse(kResetResponse, kSuccess);
                    CyDelay(1000);
                    NVIC_SystemReset();
                }
                else {
                    SendResponse(subtype, kInvalidRequest);
                }
            }
        }
    }
    
    void GetPowerStatus(){
        UART_PutString("In GetPowerStatus");
        UART_PutString("\n\r\n\r");
        getPowerStatus = false;
        while (!getPowerStatus) {
             // Check for input data from host. 
            if (0u != USBUART_DataIsReady())
            {
                // Read received data and re-enable OUT endpoint. 
                count = USBUART_GetAll(bufferIN);
                subtype = bufferIN[8];
                if (subtype ==kPowerStatus) //start operation
                {                    
                    powerStatus = GetPowerStatusFromUSB();
                    SendResponse(kPowerStatusResponse, kSuccess);
                    getPowerStatus=true;
                }
                else if (subtype==kReset){
                    UART_PutString("Reset was requested");
                    UART_PutString("\n\r\n\r");
                    SendResponse(kResetResponse, kSuccess);
                    CyDelay(1000);
                    NVIC_SystemReset();
                }
                else {
                    SendResponse(subtype, kInvalidRequest);
                }
            } 
        }
    }
    
    enum Next_State WaitForSelfTest(){
        UART_PutString("In WaitForSelfTest");
        UART_PutString("\n\r\n\r");
        
        while (_running) {
               // Check for input data from host. 
            if (0u != USBUART_DataIsReady())
            {
                // Read received data and re-enable OUT endpoint. 
                count = USBUART_GetAll(bufferIN);
                subtype = bufferIN[8];
                switch(subtype) {
                    
                    case kSelfTest:
                    //TBD self test
                    SendResponse(kReturnSelfTestRes, kSuccess); //TBD add true to the msg
                    return kEBoxParams;
                    break;
                    
                    case kGetFWVersion:
                    FWVersion();
                    SendFWVersion(&fwVersion,kSuccess);                    
                    break;
                    
                    case kReset:
                    UART_PutString("Reset was requested");
                    UART_PutString("\n\r\n\r");
                    SendResponse(kResetResponse, kSuccess);
                    CyDelay(1000);
                    NVIC_SystemReset();
                    break;
                    
                    default:
                    SendResponse(subtype, kInvalidRequest);
                    break;               
                }               
            }
        }
        
    return kReadyToStart; //TBD kIDLE ?
    }
  enum Next_State EBoxParamsState(){
    while (_running) {
                       // Check for input data from host. 
            if (0u != USBUART_DataIsReady())
            {
                // Read received data and re-enable OUT endpoint. 
                count = USBUART_GetAll(bufferIN);
                subtype = bufferIN[8];
                switch(subtype) {
                    
                    case kSelfTest:
                    //TBD self test ?
                    SendResponse(kSelfTest, kSuccess); //TBD add true to the msg
                    return kWaitForSelfTest;
                    
                    break;
                    
			        case kGetEBoxParams:            
                    EBoxParams();
                    SendEBoxParams(&e_box_params, kSuccess); 
                    break; 
                    
                    case kGotEBoxParams:
                    SendResponse(kGotEBoxParamsResponse, kSuccess);
                    return kOverlayParams;
                    break;
                    
                    case kReset:
                    UART_PutString("Reset was requested");
                    UART_PutString("\n\r\n\r");
                    SendResponse(kResetResponse, kSuccess);
                    CyDelay(1000);
                    NVIC_SystemReset();
                    break;
                    
                    default:
                    SendResponse(subtype, kInvalidRequest);
                    break;               
                }               
            }   
    }
    return kReadyToStart; //TBD kIDLE ?
}


enum Next_State OverlayParams(){
    uint8* Lut_Buffer;
    UART_PutString("In OverlayParams");
    UART_PutString("\n\r\n\r");
    while (_running) {
                       // Check for input data from host. 
        if (0u != USBUART_DataIsReady())
        {
        // Read received data and re-enable OUT endpoint. 
            count = USBUART_GetAll(bufferIN);
            subtype = bufferIN[8];
            switch(subtype) {
                
                case kReset:
                UART_PutString("Reset was requested");
                UART_PutString("\n\r\n\r");
                SendResponse(kResetResponse, kSuccess);
                CyDelay(1000);
                NVIC_SystemReset();
                break;
                
                case kGetChecksum:
                SendCheckSumResponse(kReturnCheckSum, kSuccess,(uint8)kSuccess);
                break;
                
    			case kGotOverlayParams:
                SendResponse(kGotOverlayParamsResponse, kSuccess);
    			return kReadyToStart;
                break;
                
    			case kGetMatParamsAtOnce:
                MatParams();
                SendMatParams(&mat_params, kSuccess);  
                //SendResponse(kGetMatParamsAtOnceResponse, kSuccess);                
                break;
                
                case kGetLutAtOnce:             
                for (uint8 i =0 ; i<mat_params.overlaySizeRows; i++)
                {
                    PutLut();
                    lut.CurrentRow=i;
                    SendLutToUSB(&lut,mat_params.overlaySizeCols+1/* +ref col*/,kSuccess);
                    CyDelay(20);
                }
             
                break;
                
                case kWriteMatParamsToFlash:
                GetMatParamFromUSB(&MAT_Parameters);
                WriteMatParamsToFlash(&MAT_Parameters);
                ReadMatParamsFromFlash(&MAT_Parameters);
                convert_by_subtraction(MAT_Parameters.checkSum_1, temp);
                UART_PutString("\n\r");
                UART_PutString("checkSum_1 : ");
                UART_PutArray(temp,4);
                UART_PutString("\n\r");                
                break;
                
                default:
                SendResponse(subtype, kInvalidRequest);
                break;  
                
            }
        }
    
    }
    return kReadyToStart; //TBD kIDLE ?
}

enum Next_State ReadyToStart(){
    UART_PutString("In ReadyToStart");
    UART_PutString("\n\r\n\r");
    while (_running) {
                       // Check for input data from host. 
        if (0u != USBUART_DataIsReady())
        {
        // Read received data and re-enable OUT endpoint. 
            count = USBUART_GetAll(bufferIN);
            subtype = bufferIN[8];
            switch(subtype) {
                
                case kReset:
                UART_PutString("Reset was requested");
                UART_PutString("\n\r\n\r");
                SendResponse(kResetResponse, kSuccess);
                CyDelay(1000);
                NVIC_SystemReset();
                break;
                
    			case kStart:              
    			return kNormalOperation;
                break;
                
                default:
                SendResponse(subtype, kInvalidRequest);
                break;  
                
            }
        }
    
    }
    return kReadyToStart; //TBD kIDLE ?    
}

enum Next_State RegularOperation(){
    UART_PutString("In RegularOperation");
    UART_PutString("\n\r\n\r");
    uint8 currentRow;
    uint8 currentCol; /// with out ref col
    uint8 NumOfRows=mat_params.overlaySizeRows; //TBD
    uint8 NumOfCols=mat_params.overlaySizeCols; //TBD    
    uint8 I2C_Status;
    // send last Col num To TI //////
    if(SendLastOfCol(NumOfCols-1)!=kSuccess||timeOut==true){
    Timer_1_Stop();
    TI_Comm_Fail=true;       
    }
        
    while (_running) {
                
        ////// TI Rows Switching ////
        UART_PutString("Start Mat");
        UART_PutString("\n\r"); 
        for (currentRow=0; currentRow<NumOfRows; currentRow++){
            if ((LineSwitching(kSwitchRow ,currentRow))!=kSuccess||timeOut==true){
                Timer_1_Stop();
                TI_Comm_Fail=true;
                /// TBD send error msg to tablet 
                return kCommunicationFail;
            }
            
                for (currentCol=0; currentCol<NumOfCols+1; currentCol++){ //+1 - ref col
                    ///    UART_PutString("New Col ");
                     //   UART_PutString("\n\r\n\r");
                    i2cStatus =SwitchCol(currentCol);
                    if (SwitchCol(currentCol)!= kSuccess|| timeOut==true ){
                    Timer_1_Stop();
                    TI_Comm_Fail = true;
                    return kCommunicationFail;
                    //TBD send error msg to tablet
                    }                   
                       
                    CyDelayUs(10); //TBD remove ?
                    measure(currentCol);
                }  
                Col_Control_Reg_0_Write(Reg_default_state); // turn off the CY cols
                Col_Control_Reg_1_Write(Reg_default_state); // turn off the CY cols
            ///////////////Send Results////////////////////
            SendRowMeas(currentRow,NumOfCols+1); //+ref col         
            }      
        
        
       // Check for input data from host. 
        if (0u != USBUART_DataIsReady())
        {
        // Read received data and re-enable OUT endpoint. 
            count = USBUART_GetAll(bufferIN);
            subtype = bufferIN[8];
            switch(subtype) {
                
                case kReset:
                UART_PutString("Reset was requested");
                UART_PutString("\n\r\n\r");
                SendResponse(kResetResponse, kSuccess);
                CyDelay(1000);
                NVIC_SystemReset();
                break;
                
                default:
                SendResponse(subtype, kInvalidRequest);
                break;  
                
            }
        }            
    }

    return kReadyToStart; //TBD kIDLE ?    
}
void FWVersion(){
    fwVersion.FW_Major_Version=4;
    fwVersion.FW_Minor_Version=0;
    fwVersion.FW_Build_Version=0;
}

void EBoxParams() {  
    FWVersion();
    e_box_params.adcFactor = 99;// (float)((float)2.5/(float)4096);
    e_box_params.acFreq = 99;
    e_box_params.acPk2Pk = 99;   
    e_box_params.rload = 99;
    e_box_params.FWVersionMajor = fwVersion.FW_Major_Version;
    e_box_params.FWVersionMinor = fwVersion.FW_Minor_Version;
    e_box_params.FWVersionBuild = fwVersion.FW_Build_Version;    
    e_box_params.PCBVersion =30;
    e_box_params.constCapColumn =99;
    e_box_params.constCapValue =8.2e-12;
    e_box_params.maxRowIndex = 70;
    e_box_params.maxColumnIndex = 36;
}

void  MatParams(){
    ///TBD fill real params
    
    mat_params.overlayVersionMajor = 1;
    mat_params.overlayVersionMinor = 2;
    mat_params.overlayCellSizeX = 3;
    mat_params.overlayCellSizeY = 4;
    mat_params.overlayId = 5;
    mat_params.overlayLife = 6;
    mat_params.overlaySizeCols =18; /// without ref col
    mat_params.overlaySizeRows =18;
    mat_params.overlayManufacturingDate = 9;
    mat_params.overlayFirstActivationDate = 10;
    mat_params.codeRedStatus =11;
    mat_params.codeRedIndex = 12;
    mat_params.overlayCustomerLife = 13;
    mat_params.overlayCustomerActivationDate=14;
}
void PutLut(){  
    //TBD fill real params from flash
    lut.numOfCoef = 4;
    for (uint8 i =0 ; i < mat_params.overlaySizeCols+1/*+ ref col*/ ; i++){
    lut.coefList[i*4] = coef1;
    lut.coefList[i*4+1] =coef2;
    lut.coefList[i*4+2] =coef3;
    lut.coefList[i*4+3] =coef4;
    }
}
void measure(uint8 currentCol){
    uint8 NumOfCols=mat_params.overlaySizeCols; //TBD     
    uint8 currentSample;    
    uint16 output;
    uint16 data=0;
    for (currentSample=0; currentSample<NumOfSamples ; currentSample++){                     
      //while(!COMP_OUT_Read()){
        if(ADC_DelSig_1_IsEndConversion(ADC_DelSig_1_WAIT_FOR_RESULT)){
            output = ADC_DelSig_1_GetResult16();  
          //  convert_by_subtraction(output, temp);
          //  UART_PutArray(temp,4);
          //  UART_PutString("\n\r\n\r");

          // output = ADC_DelSig_1_Read16();
            if (data<output){
                data=output;
            } 
        }                            
    }
    bufferOUT[currentCol*2+12]=(data>>8)&0xff; //MSB
    bufferOUT[currentCol*2+11]= data&0x00ff;    //LSB
    
   
            
    data=0;
    
}


enum I2C_Status SwitchCol(uint8 colNumber){
    //////Cypress Cols Switching , last col = ref col////
    if(colNumber<29||colNumber==mat_params.overlaySizeCols){  
        if(colNumber==mat_params.overlaySizeCols){  /// ref col switch
        //currentCol=RefCol;
        Col_Control_Reg_0_Write(Reg_default_state);
       // Col_Control_Reg_1_Write(Reg_default_state);
        Col_Control_Reg_1_Write(Reg1_RefCol_state); 
       // Col_29_Write(1);
       // UART_PutString("\n\r\n\r"); 
       // UART_PutString("Ref col ");
       // UART_PutString("\n\r\n\r"); 
        }
        else{
        //    Col_29_Write(0);
            if(colNumber<15){     /// cols 0 to 14 switched in reg0
            Col_Control_Reg_0_Write(colNumber+1); // 0 - default state
            Col_Control_Reg_1_Write(Reg_default_state);
            }
            else{                 /// cols 15 to 28 switched in reg1
            Col_Control_Reg_0_Write(Reg_default_state); 
            Col_Control_Reg_1_Write(colNumber-DemultiplexerNumOfSwitches+1); // 0 - default state
            }
        }

    }
    else{  ////// TI cols Switching ////
    Col_Control_Reg_0_Write(Reg_default_state); // turn off the CY cols
    Col_Control_Reg_1_Write(Reg_default_state); // turn off the CY cols
    if(( LineSwitching(kSwitchCol,colNumber)!=kSuccess)||timeOut==true){
        TI_Comm_Fail=true;
        return (enum I2C_Status) kFailed;
    }
    }
    Timer_1_Stop();
    return (enum I2C_Status)kSuccess; 
}


CY_ISR(InterruptHandler)
{
	/* Read Status register in order to clear the sticky Terminal Count (TC) bit 
	 * in the status register. Note that the function is not called, but rather 
	 * the status is read directly.
	 */
   	Timer_1_STATUS;
    UART_PutString("\n\r");
    UART_PutString("Time Out ");
    timeOut=true;
    Timer_1_Stop();
    Timer_1_WriteCounter(TimerCounter);
}




