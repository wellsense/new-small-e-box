/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/

/* [] END OF FILE */
#include <project.h>

#pragma pack(1)
struct MAT_parameters{
	uint32_t checkSum_1;
	uint32_t MT_ID;
	uint8_t  NumOfRows;
	uint8_t  NumOfCols;
	uint8_t  HWMajor;
	uint8_t  HWMinor;
	uint32_t LifeTime;
	uint32_t FirstActivationDate;
	uint8_t  codeRedStatus; // 1  - code red activated
	uint16_t codeRedIndex; //num of measures in code red
	uint32_t ManufacturingDate;
	uint16_t numOfCoef;
};

#pragma pack(1)
struct Lut_temp{	
	uint16_t numOfCoef;
    uint16_t CurrentRow;
	float coefList[200]; 
};

#pragma pack(1)
struct MAT_Lut{
	uint8_t RowNum;
	uint16_t numOfCols;
	float coefList[200]; //kCoefNum*numOfCols
};

#pragma pack(1)
struct MAT_RefRow{
	uint8_t lineNumber;
	uint8_t Length;
	float cap[40];
};

#pragma pack(1)
struct MAT_RefCol{
	uint8_t lineNumber;
	uint8_t Length;
	float cap[72];
};

struct FW_Details {
    uint8 FW_Major_Version;
    uint8 FW_Minor_Version;
    uint8 FW_Build_Version;    
};
#pragma pack(1)
struct MatParams{
	uint16 overlayVersionMajor;
	uint16 overlayVersionMinor;
	uint8  overlayCellSizeX;
	uint8  overlayCellSizeY;
	uint32 overlayId;
	uint32 overlayLife;
	uint8  overlaySizeCols;
	uint8  overlaySizeRows;
	uint32 overlayManufacturingDate;
	uint32 overlayFirstActivationDate;
	uint8  codeRedStatus; // 1  - code red activated
	uint16 codeRedIndex; //num of measures in code red
	uint32 overlayCustomerLife;
	uint32 overlayCustomerActivationDate;

};
#pragma pack(1)
struct EBoxParams{

	float adcFactor;
	float acFreq;
	uint16 acPk2Pk;
	uint32 rload;
	uint16 FWVersionMajor;
	uint16 FWVersionMinor;
    uint16 FWVersionBuild;
        //HW params
	uint32 PCBVersion;
	uint32 constCapColumn;
	float constCapValue;
	uint32 maxRowIndex;
	uint32 maxColumnIndex;
};


#pragma pack(1) //tells  compiler not to optimiaze the struct
struct HwcPowerConfig{
	//HwcPowerConfig():refreshRate(0.5f),batteryImageDelay(0.0f),LowBatteryImageDelay(0.0f){}
	float refreshRate;
	float batteryImageDelay;
	float codeRedImageDelay;
};

/**
 * Category emun - the categories of the messages over the USB protocol
 */
enum Category{
	kRequest  		 = 1,
	kResponse 		 = 2,
	kEvent    		 = 3,
	kEventNoResponse = 4
};

// TODO - CR (Future) - Create separate enumerators per request, response, and event.
/**
 * Subtype emun - includes all the subtypes of messages.
 */
enum SubType{
	kEmptySubType					,//		= 0,
			kStart 				,//			= 1,
			kStop  				,//			= 2,
			kSelfTest			,//			= 3,
			kGetOverlayId 			,//		= 4,
			kGetCodeRedData 		,//		= 5,
			kGetOverlayLife 		,//		= 6,
			kSetOverlayLife 		,//		= 7,
			kGetOverlayLut 			,//		= 8,
			kFlashMemoryCleared		,//		= 9,
			kMeasureSpecificArea 		,//	= 10,
			kGetAcFreq 			,//			= 11,
			kGetOverlayManDate 		,//		= 12,
			kPeriodicTest 			,//		= 13,
			kError 				,//			= 14,
			kLogMessage 			,//		= 15,
			kReturnOverlayId 		,//		= 16,
			kGetCodeRedDataResponse 		,//		= 17,
			kReturnOverlayLife 		,//		= 18,
			kReturnOverlayLut 		,//		= 19,
			kReturnOverlaySize 		,//		= 20,
			kReturnAcFreq 			,//		= 21,
			kReturnOverlayManDate		,//	= 22,
			kOverlayDisconnected 		,//	= 23,
			kOverlayReconnected 		,//	= 24,
			kReturnRow			,//			= 25,
			kReturnSelfTestRes 		,//		= 26,
			kGetFWVersion 			,//		= 27,
			kReturnFWVersion 		,//		= 28,
			kGetCellSize			,//		= 29,
			kReturnCellSize			,//		= 30,
			kGetAdcFactor			,//		= 31,
			kReturnAdcFactor		,//		= 32,
			kReady				,//			= 33,
			kStarted			,//			= 34,
			kStopped			,//			= 35,
			kGotOverlayParams		,//		= 36,
			kGotOverlayParamsResponse	,//= 37,
			kReturnPeriodicTestResults	,//= 38,
			kGetOverlayVersion		,//	= 39,
			kReturnOverlayVersion		,//= 40,
			kSetOverlayLifeResponse		,//= 41,
			kGetRl				,//		= 42,
			kReturnRl			,//		= 43,
			kGetCp				,//		= 44,
			kReturnCp			,//		= 45,
			kGetAcPk2Pk			,//		= 46,
			kReturnAcPk2Pk			,//	= 47,
			kGetFirstActDate		,//	= 48,
			kReturnFirstActDate		,//	= 49,
			kGetChecksum			,//	= 50,
			kReturnCheckSum			,//	= 51,
			kSetFirstActDate		,//	= 52,
			kSetFirstActDateResponse	,//= 53,
			kReadyResponse			,//	= 54,
			kKeepAliveResponse		,//	= 55,
			kOlayEepromBurnData		,//	= 56,
			kOlayEepromBurnDataResponse 	,//	= 57,
			kRequestOlayEepromCurrentData 	,// = 58,
			kReturnOlayEepromCurrentData 	,//= 59,
			kGotEBoxParams		,//	= 60,
			kGotEBoxParamsResponse	,//= 61,
			kGotoHwcParams			,//	= 62,
			kGotoHwcParamsResponse		,//= 63,
			KGotEventResponse		,//	= 64,
			kGetConstAmp			,//	= 65,
			kReturnConstAmp			,//	= 66,
			kGetConstCap			,//	= 67,
			kReturnConstCap			,//	= 68,
			kReset				,//		= 69,
			kResetResponse			,//	= 70,
			kGetEBoxParams   		,//	= 71,
			kGetEBoxParamsResponse	,//= 72,
			kGetMatParamsAtOnce		,//	= 73,
			kGetMatParamsAtOnceResponse	,//= 74,
			kGetLutAtOnce			,//	= 75,
			kGetLutAtOnceResponse		,//= 76,
			kKeepAlive			,//		= 77,
			kUpgradeHwc                     ,// = 78
			kUpgradeHwcAck                  ,// = 79
			kUpgradeHwcVerified            ,// = 80
			kUpgradeHwcVerifiedAck         ,// = 81
			kStartImageTransferSession         	,// = 82
			kStartImageTransferSessionResponse	,// = 83
			kEndImageTransferSession         	,// = 84
			kEndImageTransferSessionResponse 	,// = 85
			kCheckUpgradeValidity		        ,// = 86
			kCheckUpgradeValidityResponse           ,// = 87
			kStartImageTransfer			,// = 88
			kStartImageTransferResponse		,// = 89
			kSendNextImageBlock			,// = 90
			kSendNextImageBlockResponse		,// = 91
			kEndImageTransfer			,// = 92
			kEndImageTransferResponse		,// = 93
			kPowerStatus                            ,//=94
			kPowerStatusResponse                    ,//=95
			kPowerPlugged                           ,//=96
			kPowerUnplugged                         ,//=97
			kPowerConfig							,//TODO: add in final code 98
			kPowerConfigResponse					,//=99
			kHwcUpgradeRequest,                      //=100
			kClearFlashMemory,                      //101
			kGetRefRow,                             //102
			kGetRefCol,                             //103
			kGetRefRowResponse,                     //104
			kGetRefColResponse,                     //105
			kSetCustomerActDate,					//106
			kSetCustomerActDateResponse,			//107
			kSetCustomerOverlayLife,                //108
			kSetCustomerOverlayLifeResponse,        //109
            kWriteLutToFlash, 						//110
			kWriteLutToFlashResponse,               //111
			kWriteMatParamsToFlash,                    //112
			kWriteMatParamsToFlashRsponse,             //113
			kWriteRefRowToFlash, 					//114
			kWriteRefRowToFlashResponse,            //115
			kWriteRefColToFlash,                    //116
			kWriteRefColToFlashResponse,            //117
			kCheckFlashWriting,                     //118
			kCheckFlashWritingResponse,             //119
			kUpdateCheckSum,                        //120
			kUpdateCheckSumResponse,                //121
			kLastSubType				= kUpdateCheckSumResponse
};

enum HwcUpgradeParts {
    kHwcUserCode,
    kSecondStageBootLoader
};

/**
 * Status emun - represents different statuses for messages over the comm protocol
 */
enum Status{
	kSuccess 			= 0,
	kFailed	 			= 1,
	kUnknownCommand		= 2,
	kInvalidParameter	= 3,
	kTimeout			= 4,
	kDisconnect			= 5,
	kInvalidRequest		= 6,	//will be sent from the HWC when an illegal request was recieved according to the state machine
	kMsgToLongHwc		= 7,
	kNoCallback			= 8,
	kGetNextParamOutOfBounds = 12,
	kStatus_InternalError = 13,

	kImageTooLarge,		// The total size of the transferred image blocks exceeds the maximum available size
	kInvalidImageSize,	// Transferred image size mismatches the sum of individual blocks
	kInvalidImageChecksum,	// Transferred image failed checksum verification

	kFlashProgramError,      // IAP failed
	kOverCurrentEvevnt

};

enum PowerStatus {
    kPowerStatusUnknown=-1,
    kAcPowerStatus=0,    //
	kBatteryStatus,
	kCodeRedStatus    //
};
enum I2C_Status{
    kI2C_UnknownCommand,        //=0
    kI2C_Success ,             //=1
    kI2C_Failed               //=2
};

// for EN 1.25 LDO and USB Good comm
enum I2C_ON_OFF{
    kOff  = 0,
    kOn   = 1,
};

//I2C message respond structure
typedef struct
{
    uint8_t category;
    uint8_t data_1;
}I2C_MsgToCypress;


enum I2C_Category{
    kSwitchRow,                  //=0
    kSwitchCol,                  //=1
    kSwitchOffAllRows,           //=2
    kSwitchOffAllCols,           //=3
    kSwitchOffAllLines,          //=4
    kTabletLed,                  //=5
    kLDOEn,                      //=6    
    kI2CReset,                   //=7
    kGetTIFWVersion,             //=8
    kLastCol                     //=9
};





