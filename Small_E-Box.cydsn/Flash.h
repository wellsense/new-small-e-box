/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/

/* [] END OF FILE */
#include <project.h>


// instructions
#define RDSR 0x05  // read status register
#define RDCR 0x35  // read configuration register
#define READ  0x03
#define PP 	 0x02  //page program
#define WRDI 0x04 //write disable
#define WREN 0x06 //write enable
#define RSTEN 0x66 //reset enable
#define RST_memory  0x99 //reset memory
#define RSTQIO 0XFF //
#define HIGH_SPEED_READ  0x0B //speed read
#define Erase_Full_Array 0xC7
#define ULBPR 0x98 //global block protection unlock
#define Erase_4k_Of_Memory 0X20
#define PageSize 256

////////////// Mat Parameters ////////
#define MatParamStartAddress       0x101020
#define CheckSum1Address           0x00 //0
#define MT_ID_Address              0x04 //4
#define NumOfRowsAddress           0x08 //8
#define NumOfColsAddress           0x09 //9
#define HWMajorAddress             0x0a //10
#define HWMinorAddress             0x0b //11
#define LifeTimeAddress            0x0c //12
#define FirstActivationDateAddress 0x10 //16
#define codeRedStatusAddress       0x14 //20
#define codeRedIndexAddress        0x15 //21
#define ManufacturingDateAddress   0x17 //23
#define numOfCoefAddress           0x1b //27
#define MatRefRowAddress           0xff // TBD
#define MatRefColAddress           0xfff//TBD


#define LutStartAddress            0x100
#define SelfTestAddress            0x3D1000


