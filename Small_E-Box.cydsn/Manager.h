/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/

/* [] END OF FILE */
/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/

/* [] END OF FILE */
#include <project.h>
#include<stdio.h>
//#include<Details.h>
//#include<Communication.h>


#define RefCol  29u
#define DemultiplexerNumOfSwitches 15
#define Reg_default_state 0
#define Reg1_RefCol_state 15

enum Bool {
    kfalse,// =0
    ktrue  // = 1
};

	enum Next_State {
		kWaitForSelfTest,
		kReadyToStart,
		kNormalOperation,
		kEBoxParams,
		kOverlayParams,
		kIDLE,
		kCommunicationFail,
		kCodeRed
	};
    void StartOperation();
    void WaitForReadyFromScu();    
    void GetPowerConfig();
    void GetPowerStatus();
    void FWVersion();
    enum Next_State WaitForSelfTest();
    enum Next_State EBoxParamsState();
    enum Next_State OverlayParams();
    enum Next_State ReadyToStart();
    enum Next_State RegularOperation();
    void EBoxParams();
    void MatParams();
    void PutLut();
    void measure(uint8 currentCol);    
    enum I2C_Status SwitchCol(uint8 colNumber);
    CY_ISR(InterruptHandler);
    
    void convert_by_subtraction(int value, uint8 *temp);
    
    


    
    