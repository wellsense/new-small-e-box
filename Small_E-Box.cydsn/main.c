/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include <project.h>
#include<stdio.h>
#include<math.h>
#include <Details.h>
# include <Manager.h>
# define True = 1;
# define False = 0;
void convert_by_division(int value, uint8 *temp);
void convert_by_subtraction(int value, uint8 *temp);
void reverse(char *str, int len);
int intToStr(int x, char str[], int d);
void ftoa(float n, char *res, int afterpoint);


#if defined (__GNUC__)
    /* Add an explicit reference to the floating point printf library */
    /* to allow usage of the floating point conversion specifiers. */
    /* This is not linked in by default with the newlib-nano library. */
    asm (".global _printf_float");
#endif

#define USBFS_DEVICE    (0u)

/* The buffer size is equal to the maximum packet size of the IN and OUT bulk
* endpoints.
*/
#define USBUART_BUFFER_SIZE (64u)
#define LINE_STR_LENGTH     (20u)
#define USED_EEPROM_SECTOR          (1u)

/*******************************************************************************
* Function Name: main
********************************************************************************
*
* Summary:
*  main function performs following functions:
*   1. Start the VDAC8_1, ADC_DelSig_1 and Sample_Hold_1
*   2. Sample and Hold output is observed on LCD
*
* Parameters:
*  None.
*
* Return:
*  None.
*
*******************************************************************************/


int main()
{
    CyGlobalIntEnable;

    /* Start all the components */
    Clock_1_Start();
    Mixer_1_Start();
    Mixer_2_Start();
    Comp_1_Start();
    PGA_1_Start(); 
    UART_Start();
    EEPROM_Start();
    I2C_Start();
    SPIM_Start();   
    USBUART_Start(USBFS_DEVICE, USBUART_5V_OPERATION);    
    I2C_MasterClearStatus(); /* Clear any previous status */
    SPIM_EnableRxInt();
    SPIM_EnableTxInt();
    SPIM_ClearTxBuffer();
    SPIM_ClearRxBuffer();    
    /* Enable the Interrupt component connected to Timer interrupt */
    timeOut_StartEx(InterruptHandler);
    Timer_1_WritePeriod(24e6); //equal 1 sec
    Col_Control_Reg_0_Write(Reg_default_state); // turn off the CY cols
    Col_Control_Reg_1_Write(Reg_default_state); // turn off the CY cols    
    /* ADC is started to convert the analog Sample and Hold signal to digital*/
    ADC_DelSig_1_Start();   
    /* Start the ADC conversion */
    ADC_DelSig_1_StartConvert();


    StartOperation();
}

void convert_by_subtraction(int value, uint8 *temp)
{
    static const short subtractors[] = { 1000, 100, 10, 1 };
    int i;
    for (i = 0; i < 4; i++)
    {
        int n = 0;
        while (value >= subtractors[i])
        {
            n++;
            value -= subtractors[i];
        }
        temp[i] = n + '0';
    }
}

 
// reverses a string 'str' of length 'len'
void reverse(char *str, int len)
{
    int i=0, j=len-1, temp;
    while (i<j)
    {
        temp = str[i];
        str[i] = str[j];
        str[j] = temp;
        i++; j--;
    }
}
 
 // Converts a given integer x to string str[].  d is the number
 // of digits required in output. If d is more than the number
 // of digits in x, then 0s are added at the beginning.
int intToStr(int x, char str[], int d)
{
    int i = 0;
    while (x)
    {
        str[i++] = (x%10) + '0';
        x = x/10;
    }
 
    // If number of digits required is more, then
    // add 0s at the beginning
    while (i < d)
        str[i++] = '0';
 
    reverse(str, i);
    str[i] = '\0';
    return i;
}
// Converts a floating point number to string.
void ftoa(float n, char *res, int afterpoint)
{
    // Extract integer part
    int ipart = (int)n;
 
    // Extract floating part
    float fpart = n - (float)ipart;
 
    // convert integer part to string
    int i = intToStr(ipart, res, 0);
 
    // check for display option after point
    if (afterpoint != 0)
    {
        res[i] = '.';  // add dot
 
        // Get the value of fraction part upto given no.
        // of points after dot. The third parameter is needed
        // to handle cases like 233.007
        fpart = fpart * 10*10;//pow(10, afterpoint);
 
        intToStr((int)fpart, res + i + 1, afterpoint);
    }
}


/* [] END OF FILE */
