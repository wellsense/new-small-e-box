/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/

/* [] END OF FILE 
#include <Flash.h>
#include <project.h>
#include <stdio.h>
#include <Details.h>

//void WriteMatParamsToFlash(struct MAT_parameters* MAT_Parameters);

uint8 SPIM_status;
uint8 SPIMWriteBuf[256];

void WriteMatParamsToFlash( struct MAT_parameters* MAT_Parameters){
    uint32 address = MatParamStartAddress;
    SPIMWriteBuf[0] = (uint8)Erase_4k_Of_Memory;
    SPIMWriteBuf[1] = (address>16)&0x00ff; //MSB
    SPIMWriteBuf[2] = (address>8)&0x00ff;
    SPIMWriteBuf[3] =  address&0x0000ff;  //LSB
    SPIM_WriteTxData(WREN);
    CyDelay(100);
    SPIM_WriteTxData(ULBPR);
    CyDelay(100);
    SPIM_WriteTxData(WREN);
    CyDelay(100);
    SPIM_PutArray(SPIMWriteBuf,4); //erase the sector
    CyDelay(300);
    SPIM_WriteTxData(WREN);
    CyDelay(300);
    memcpy(SPIMWriteBuf,&MAT_Parameters,sizeof(MAT_Parameters));
   
    SPIMWriteBuf[0]=(uint8)(MAT_Parameters->checkSum_1)&0x0000ff;
    SPIMWriteBuf[1]=(uint8)((MAT_Parameters->checkSum_1)>8)&0x0000ff;
    SPIMWriteBuf[2]=(uint8)((MAT_Parameters->checkSum_1)>16)&0x0000ff;
    SPIMWriteBuf[3]=(uint8)((MAT_Parameters->checkSum_1)>24)&0x0000ff;
    
    SPIM_status=SPIM_ReadTxStatus();
        if(SPIM_status & (SPIM_STS_SPI_DONE | SPIM_STS_SPI_IDLE))
    {          
        SPIM_PutArray(SPIMWriteBuf,(uint8)MAT_Parameters); 
    }
    CyDelay(300);
    
}
    */